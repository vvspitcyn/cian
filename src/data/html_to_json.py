import pathlib

import scrape_search_results
import make_bs
import parse_offers
import to_db


# raw_folder = scrape_search_results.main()
PROJECT_FOLDER_PATH = pathlib.Path.cwd()
raw_folder = PROJECT_FOLDER_PATH / 'data/raw/2022-10-03'
bs_folder = make_bs.main(raw_folder)
json_folder = parse_offers.main(bs_folder)
to_db.main(json_folder)




