# -*- coding: utf-8 -*-
import pathlib

from bs4 import BeautifulSoup


PROJECT_FOLDER_PATH = pathlib.Path.cwd()
RAW_PAGES_FOLDER_PATH = PROJECT_FOLDER_PATH / 'data/raw'
BS_FOLDER_PATH = PROJECT_FOLDER_PATH / 'data/interim/bs'


def txt_to_bs(path: pathlib.Path):
    with open(path, 'rb') as f:
        soup = BeautifulSoup(f, features="lxml").find_all('script', type='text/javascript')[3]
    return soup.prettify()


def make_bs_date_folder(raw_folder_path):
    save_folder_path = BS_FOLDER_PATH / raw_folder_path.parts[-1]
    save_folder_path.mkdir(exist_ok=True)
    return save_folder_path


def save_bs(soup: str, source_path: pathlib.Path):
    save_folder_path = BS_FOLDER_PATH / source_path.parts[-2]
    save_file_path = save_folder_path / source_path.parts[-1]
    with open(save_file_path, 'w', encoding='utf-8') as f:
        f.write(soup)


def main(raw_folder_path: pathlib.Path):
    bs_date_folder_path = make_bs_date_folder(raw_folder_path)
    for file_path in raw_folder_path.glob('*'):
        try:
            soup = txt_to_bs(file_path)
            save_bs(soup, file_path)
        except:
            pass
    return bs_date_folder_path
    

if __name__ == "__main__":
    import sys
    raw_folder_path = RAW_PAGES_FOLDER_PATH / sys.argv[1]
    main(raw_folder_path)
