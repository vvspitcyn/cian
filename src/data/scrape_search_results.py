# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 13:53:27 2021

@author: Vyacheslav.Spitsin
"""
from datetime import datetime
import pathlib
import time

from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import requests
from scipy.stats import expon, norm

import make_bs


PROJECT_FOLDER_PATH = pathlib.Path.cwd()
RAW_PAGES_FOLDER_PATH = PROJECT_FOLDER_PATH / 'data/raw'
QUERY = 'https://www.cian.ru/cat.php?deal_type=rent&engine_version=2&offer_type=flat&p={}&region=1&room1=1&room2=1&room9=1&sort=creation_date_desc&type=4'
PAGE_LIMIT = 54


def make_today_dir():
    dir_path = RAW_PAGES_FOLDER_PATH / str(datetime.now().date())
    dir_path.mkdir(exist_ok=True)
    return dir_path


def save_page(url, content, save_folder):
    save_name = url.split('?')[-1] + '.txt'
    save_path = save_folder / save_name
    with open(save_path, 'wb') as f:
        f.write(content)


def delete_broken_files(dir_path, min_kilobytes=1000):
    for f in dir_path.glob('*'):
        if f.stat().st_size / 1024 < min_kilobytes:
            f.unlink()        


def main(first_page=PAGE_LIMIT):
    today_dir = make_today_dir()
    for page_iter in range(first_page, 0, -1):
        url = QUERY.format(page_iter)
        response = requests.get(url,
                                headers={'User-Agent': UserAgent().random})
        print(page_iter, datetime.now().strftime('%H:%M:%S'), response)
        if response.status_code != 200:
            break
        html = response.content
        soup = BeautifulSoup(html, features="lxml")
        title = soup.find('title').text
        if 'captcha' in title.lower():
            break
        save_page(url, html, today_dir)
        time.sleep(expon.rvs(112, 29) + norm.rvs(5, 7))
    delete_broken_files(today_dir)
    return today_dir


if __name__ == "__main__":
    main()
