# -*- coding: utf-8 -*-
"""
Created on Sun Nov 14 15:27:58 2021

@author: Vyacheslav.Spitsin
"""
import json
import pathlib


PROJECT_FOLDER_PATH = pathlib.Path.cwd()
BS_FOLDER_PATH = PROJECT_FOLDER_PATH / 'data/interim/bs'
PLAIN_FIELDS_PATH = PROJECT_FOLDER_PATH / 'data/anc/fields/plain.json'
DICT_FIELDS_PATH = PROJECT_FOLDER_PATH / 'data/anc/fields/dict.json'
GEO_FIELDS_PATH = PROJECT_FOLDER_PATH / 'data/anc/fields/geo.json'
SAVE_JSON_FOLDER_PATH = PROJECT_FOLDER_PATH / 'data/interim/json'

N_LOCATION_ITEMS = {
    'undergrounds': 3, 
    'highways': 2, 
    'address': 6, 
    'railways': 3
}

def read_bs(path):
    with open(path, 'r', encoding='utf-8') as f:
        bs = f.read()
        return bs


def extract_jsons(bs):
    parse_from_pos = bs.find('{\"key\":\"initialState\"')
    parse_to_pos = bs.find('</script>') - 4
    parse_string = bs[parse_from_pos:parse_to_pos]
    parsed = json.loads(parse_string)
    return parsed['value']['results']['offers']


def load_json(path):
    with open(path, 'r', encoding='utf-8') as f:
        return json.load(f)


def save_json(data, path):
    with open(path, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)


def make_date_folder(folder_name):
    dir_path = SAVE_JSON_FOLDER_PATH / folder_name
    dir_path.mkdir(exist_ok=True)
    return dir_path


def parse_plain_fields(offer: dict) -> dict:
    res = {}
    for field in plain_fields:
        try:
            res[field] = offer[field]
        except:
            res[field] = None
    return res


def parse_dict_fields(offer: dict) -> dict:
    res = {}
    for key in dict_fields:
        for field in dict_fields[key]:
            try:
                res[str(key) + '_' + str(field)] = offer[key][field]
            except:
                res[str(key) + '_' + str(field)] = None
    return res
  

def parse_coordinates(offer: dict) -> dict:
    return offer['geo']['coordinates']  


def parse_jk_fields(offer: dict) -> dict:
    jk_fields = geo_fields['jk']
    res = {}
    for d in jk_fields:
        for key in d:
            try:
                res['_'.join(['geo', str(key)])] = offer['geo']['jk'][key][d[key]]
            except:
                res['_'.join(['geo', str(key)])] = None
    return res


def parse_location_fields(offer: dict, location_data, n_items) -> dict:
    keys = geo_fields[location_data]
    fields = [f'{location_data}_{i}_' + key for i in range(n_items) for key in keys]
    res = dict.fromkeys(fields)
    for i in range(min(n_items, len(offer['geo'][location_data]))):
        location_json = offer['geo'][location_data][i]
        for key in keys:
            try:
                res[f'{location_data}_{i}_' + key] = location_json[key]
            except: pass
    return res


def parse_offer(offer: dict) -> dict:
    offer_vals = {}
    offer_vals.update(parse_plain_fields(offer))
    offer_vals.update(parse_dict_fields(offer))
    offer_vals.update(parse_coordinates(offer))
    offer_vals.update(parse_jk_fields(offer))
    for loc_data in ['undergrounds', 'highways', 'address', 'railways']:
        offer_vals.update(parse_location_fields(offer, loc_data, N_LOCATION_ITEMS[loc_data]))
    return offer_vals


plain_fields = load_json(PLAIN_FIELDS_PATH)
dict_fields = load_json(DICT_FIELDS_PATH)
geo_fields = load_json(GEO_FIELDS_PATH)

def main(bs_date_folder):
    for bs_path in bs_date_folder.glob('*'):
        print(bs_path)
        bs = read_bs(bs_path)
        offers: list[dict] = extract_jsons(bs)
        page_offers = []

        for offer in offers:
            page_offers.append(parse_offer(offer))

        json_date_folder = make_date_folder(bs_date_folder.parts[-1])
        save_json(page_offers, json_date_folder / '{}.json'.format(bs_path.stem))
    return json_date_folder


if __name__ == "__main__":
    import sys
    bs_date_folder_path = BS_FOLDER_PATH / sys.argv[1]
    main(bs_date_folder_path)