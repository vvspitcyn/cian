from copy import copy
from datetime import datetime, timezone
import json
from pathlib import Path
import pathlib
from zoneinfo import ZoneInfo

import psycopg2
from psycopg2.extras import execute_batch


PROJECT_FOLDER = Path('C:/Users/Vyacheslav.Spitsin/Documents/PythonScripts/moscow_rent')
JSON_FOLDER = PROJECT_FOLDER / 'data/interim/json'

STR_TO_FLOAT_FIELDS = [
                        'kitchenArea',
                        'totalArea',
                        'livingArea'
                        ]


def read_json_list(json_list_path):
    with open(json_list_path, 'r', encoding='utf8') as f:
        json_list = json.load(f)
    return json_list


def transform_json(source_json, scraping_date):
    transformed_json = copy(source_json)
    transformed_json['creationDate'] = (datetime.strptime(transformed_json['creationDate'].split('.')[0], 
                                                        '%Y-%m-%dT%H:%M:%S')
                                                        .replace(tzinfo=ZoneInfo('Europe/Moscow')))
    transformed_json['addedDatetime'] = datetime.fromtimestamp(transformed_json['addedTimestamp'], 
                                                                tz=ZoneInfo('Europe/Moscow'))
    for field in STR_TO_FLOAT_FIELDS:
        if type(transformed_json[field]) == str:
            transformed_json[field] = float(transformed_json[field])
    if transformed_json['lat'] < transformed_json['lng']:
        tmp_lng = transformed_json['lng']
        transformed_json['lng'] = transformed_json['lat']
        transformed_json['lat'] = tmp_lng
    transformed_json['scraping_date'] = datetime.strptime(scraping_date, '%Y-%m-%d')
    return transformed_json


def main(date_json_folder: pathlib.Path):
    date_json_list = []
    date_ids = set()
    print(date_json_folder.name)
    for page_num, json_list_path in enumerate(date_json_folder.iterdir()):
        print(json_list_path)
        transformed_json_list = []
        for item in read_json_list(json_list_path):
            if item['id'] in date_ids:
                continue 
            transformed_json = transform_json(item, date_json_folder.name)  
            transformed_json['page'] = json_list_path.name
            transformed_json_list.append(transformed_json)
            date_ids.add(item['id'])
        date_json_list.extend(transformed_json_list)

    columns = list(date_json_list[0].keys())
    records = [list(d.values()) for d in date_json_list]

    with psycopg2.connect(dbname="cian", user="postgres", password="s250796") as conn:
        cur = conn.cursor()
        execute_batch(cur,
                        '''INSERT INTO raw_rent({}) VALUES ({})'''
                        .format(', '.join(columns), ', '.join(['%s']*len(records[0]))),
                        records)
        conn.commit()
        cur.close()
    conn.close()

if __name__ == "__main__":
    import sys
    date_json_folder = JSON_FOLDER / sys.argv[1]
    main(date_json_folder)