import pathlib
import requests

import pandas as pd


PROJECT_FOLDER_PATH = pathlib.Path.cwd()
SAVE_FOLDER_PATH = PROJECT_FOLDER_PATH / 'data/interim'
# KM_ZERO = {'lat': 55.755919, 'lng': 37.617589}
city_id_map = {1: 'Москва', 2: 'Санкт-Петербург'}

def get_response(city_id):
    url = f'https://api.hh.ru/metro/{city_id}'
    response = requests.get(url).json()
    return response['lines']

cities_stations = []
for city_id, city_name in city_id_map.items():
    city_stations = pd.json_normalize(get_response(city_id), 'stations', ['id', 'hex_color', 'name'], meta_prefix='line_', sep='_')
    city_stations['city'] = city_name
    cities_stations.append(city_stations)

stations_df = pd.concat(cities_stations, ignore_index=True)
stations_df.to_csv(SAVE_FOLDER_PATH / 'stations.csv', index=False)