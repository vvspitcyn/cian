    CREATE TABLE moscow_rent AS
WITH flat_station_rank AS (
SELECT * --, array_agg(station_distance) -- id, station_name, line_name, n, station_distance
FROM (SELECT f.*,
            s.id                                                                        AS station_id,
            s.name                                                                      AS station_name,
            s.lat                                                                       AS station_lat,
            s.lng                                                                       AS station_lng,
            st_order,
            line_id,
            line_hex_color,
            line_name,
            cian_name                                                                   AS station_cian_name,
            earth_distance(ll_to_earth(f.lng, f.lat),
                           ll_to_earth(s.lng, s.lat))                                   AS station_distance,
            dense_rank()
            OVER (PARTITION BY f.id ORDER BY earth_distance(ll_to_earth(f.lng, f.lat),
                                                            ll_to_earth(s.lng, s.lat))) AS station_n
     FROM (SELECT *
           FROM (SELECT *,
                        row_number()
                        OVER (PARTITION BY id, bargainterms_pricerur ORDER BY addeddatetime) AS publication_num
                 FROM raw_rent
                 WHERE address_0_fullName = 'Москва') AS fst_pub
           WHERE publication_num = 1
           --LIMIT 200
           ) AS f
              CROSS JOIN (SELECT *
                          FROM (SELECT *, row_number() OVER (PARTITION BY name ORDER BY line_name != 'Кольцевая') AS num
                                FROM station
                                WHERE city = 'Москва') AS station_num
                          WHERE num = 1
                          ) AS s
--          ON f.address_0_name = s.city
--          AND earth_distance(ll_to_earth(f.lng, f.lat),  ll_to_earth(s.lng, s.lat)) < 2000
    ) AS fs
WHERE station_n < 4
--GROUP BY id, bargainterms_pricerur
--ORDER BY addeddatetime, station_n
),
station AS (
SELECT scraping_date, id, --lat, lng,
       distance_arr[1] AS station1_distance, distance_arr[2] AS station2_distance, distance_arr[3] AS station3_distance,
       station_lat_arr[1] AS station1_lat, station_lng_arr[1] AS station1_lng,
       station_lat_arr[2] AS station2_lat, station_lng_arr[2] AS station2_lng,
       station_lat_arr[3] AS station3_lat, station_lng_arr[3] AS station3_lng,
       station_arr[1] AS station1, station_arr[2] AS station2, station_arr[3] AS station3,
       station_cian_name_arr[1] AS station1_cian_name, station_cian_name_arr[2] AS station2_cian_name, station_cian_name_arr[3] AS station3_cian_name,
       line_name_arr[1] AS line1_name, line_name_arr[2] AS line2_name, line_name_arr[3] AS line3_name
FROM (
    SELECT scraping_date, id, --, lat, lng,
           array_agg(station_distance ORDER BY station_n) AS distance_arr,
           array_agg(station_lat ORDER BY station_n) AS station_lat_arr,
           array_agg(station_lng ORDER BY station_n) AS station_lng_arr,
           array_agg(station_name ORDER BY station_n) AS station_arr,
           array_agg(station_cian_name ORDER BY station_n) AS station_cian_name_arr,
           array_agg(line_name ORDER BY station_n) AS line_name_arr,
           array_agg(station_id ORDER BY station_n) AS station_id_arr
    FROM flat_station_rank
    GROUP BY scraping_date, id --, lat, lng
    ) AS agg_st
)

SELECT uniq_f.*,
       station1, station2, station3,
       station1_distance, station2_distance, station3_distance,
       station1_lat, station1_lng, station2_lat, station2_lng, station3_lat, station3_lng,
       station1_cian_name, station2_cian_name, station3_cian_name,
       line1_name, line2_name, line3_name
FROM (
    SELECT *
    FROM flat_station_rank
    WHERE station_n = 1
    ) AS uniq_f
    LEFT JOIN station
        ON uniq_f.id = station.id
            AND uniq_f.scraping_date = station.scraping_date
ORDER BY addeddatetime